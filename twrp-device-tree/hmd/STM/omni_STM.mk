#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from STM device
$(call inherit-product, device/hmd/STM/device.mk)

PRODUCT_DEVICE := STM
PRODUCT_NAME := omni_STM
PRODUCT_BRAND := Nokia
PRODUCT_MODEL := Nokia G400 5G
PRODUCT_MANUFACTURER := hmd

PRODUCT_GMS_CLIENTID_BASE := android-hmd

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="Styles_04US-user 12 SKQ1.220201.001 04US_1_22D release-keys"

BUILD_FINGERPRINT := Nokia/Styles_04US/STM:12/SKQ1.220201.001/04US_1_22D:user/release-keys
