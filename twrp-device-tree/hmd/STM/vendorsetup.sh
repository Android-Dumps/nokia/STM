#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

add_lunch_combo omni_STM-user
add_lunch_combo omni_STM-userdebug
add_lunch_combo omni_STM-eng
