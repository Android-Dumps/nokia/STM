#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_STM.mk

COMMON_LUNCH_CHOICES := \
    lineage_STM-user \
    lineage_STM-userdebug \
    lineage_STM-eng
